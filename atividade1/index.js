function retornaNumero(){
    return new Promise((resolve,reject) => {
        setTimeout(() => {
          const aleatorio = Math.floor(Math.random() * 10) +1 ;
          if(aleatorio <5){
            resolve(aleatorio);
          }
          else{
            reject("Erro");
          }
        }, 3000);
      });
}

async function principal() {
    const resultado = await retornaNumero();

    return resultado;
}

principal();